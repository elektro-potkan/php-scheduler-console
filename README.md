Scheduler - Console
===================

Part of heavily modified fork of [contributte/scheduler](https://github.com/contributte/scheduler).

Adds support to run the scheduler from console (e.g. via system cron daemon).


Usage
-----

### Setup
This package relies on `symfony/console`.

The simplest way is to use the `elektro-potkan/scheduler-di` package
to register the console commands into Nette DI
and then the `contributte/console` integration to automatically
bundle the DI-registered commands into a console application.

```bash
composer require contributte/console
composer require elektro-potkan/scheduler-di
```

Register the extension into Nette DI via NEON config file:
```neon
extensions:
	console: Contributte\Console\DI\ConsoleExtension(%consoleMode%)
```

### System cron daemon
Set-up crontab to run the scheduler each minute.
Use the `scheduler:run` command.
```
* * * * * php path-to-project/console scheduler:run
```

### Commands
After completing the [setup](#setup), You can fire one of these commands.

| Command              | Info                              |
|----------------------|-----------------------------------|
| scheduler:help       | Print cron syntax.                |
| scheduler:list       | List all jobs.                    |
| scheduler:run        | Run all due jobs.                 |
| scheduler:force-run  | Force run selected scheduler job. |


Authors
-------
- Modified fork by Elektro-potkan <git@elektro-potkan.cz>.
- Original Contributte package authors (see [Composer config file](composer.json)):
  - [Milan Felix Šulc](https://f3l1x.io)
  - [Josef Benjac](http://josefbenjac.com)


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
You may use this program under the terms of the MIT License.

See file [LICENSE](LICENSE).
