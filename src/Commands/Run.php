<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ElektroPotkan\Scheduler\IScheduler;


class Run extends Command {
	/** @var string */
	protected static $defaultName = 'scheduler:run';
	
	/** @var IScheduler */
	private $scheduler;
	
	
	public function __construct(IScheduler $scheduler){
		parent::__construct();
		$this->scheduler = $scheduler;
	} // constructor
	
	protected function configure(): void {
		$this->setName(self::$defaultName)
			->setDescription('Run scheduler jobs');
	} // configure
	
	protected function execute(InputInterface $input, OutputInterface $output): int {
		$this->scheduler->run();
		
		return Command::SUCCESS;
	} // execute
} // class Run
